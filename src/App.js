import React, { Component } from "react";
import "./App.css";
import "./Animations.css";
import PersonalInfo from "./components/PersonalInfo/PersonalInfo";
import ContentInfo from "./components/ContentInfo/ContentInfo";

class App extends Component {
  render() {
    return (
      <div className="App">
        <body>
          <div className="Body">
            <div className="Info-div">
              <PersonalInfo />
            </div>
            <div className="Content-div">
              <ContentInfo />
            </div>
          </div>
        </body>
      </div>
    );
  }
}

export default App;
