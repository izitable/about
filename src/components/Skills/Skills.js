import React, { Component } from "react";
import SkillBar from "../SkillBar/SkillBar";
import "./styles.css";

class Skills extends Component {
  constructor(props) {
    super(props);
    this.state = {
      skills: [
        { skill: "ReactJS", value: 6 },
        { skill: "React-Native", value: 6 },
        { skill: "Redux", value: 6 },
        { skill: "Javascript", value: 6 },
        { skill: "HTML/CSS", value: 5 },
        { skill: "Java", value: 6 },
        { skill: "Python", value: 5 },
        { skill: "C#/.NET", value: 5 },
        { skill: "Selenium", value: 4 },
        { skill: "English", value: 8 }
      ]
    };
  }

  render() {
    return (
      <div>
        <div class="Skills-div">
          <h1 className="Title">Skills</h1>
        </div>
        <div class="Info-container">
          {this.state.skills.map(item => <SkillBar skill={item} />)}
        </div>
      </div>
    );
  }
}

export default Skills;
