import React, { Component } from "react";
import "./styles.css";

class SkillBar extends Component {
  render() {
    var width = this.props.skill.value * 10 + "%";
    return (
      <div className="Skill-div">
        <div className="Text-div">
          <p>{this.props.skill.skill}</p>
        </div>
        <div className="Bar-div">
          <div className="Bar-content stretchRight" style={{ width: width }}>
            <p className="Bar-text">{width}</p>
          </div>
        </div>
      </div>
    );
  }
}

export default SkillBar;
