import React, { Component } from "react";
import Skills from "../Skills/Skills";
import Experience from "../Experience/Experience";
import Academic from "../Academic/Academic";
import Portfolio from "../Portfolio/Portfolio";
import "./styles.css";

class ContentInfo extends Component {
  render() {
    return (
      <div className="Card-container">
        <div className="Card">
          <Skills />
        </div>
        <div className="Card">
          <Experience />
        </div>
        <div className="Card">
          <Academic />
        </div>
        <div className="Card">
          <Portfolio />
        </div>
      </div>
    );
  }
}

export default ContentInfo;
