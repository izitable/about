import React, { Component } from "react";
import Info from "../Info/Info";
import "./styles.css";

class PersonalInfo extends Component {
  render() {
    return (
      <div className="Personal-info">
        <div
          style={{ flex: 1, alignItems: "center", justifyContent: "center" }}
        >
          <div className="Profile-div">
            <img
              alt="me"
              className="Profile"
              src="https://scontent.fopo2-1.fna.fbcdn.net/v/t1.0-9/1507620_10151802380396511_304969229_n.jpg?oh=fd012ca1116d38dff6d02835a9a5f940&oe=5A6F238E"
            />
          </div>
        </div>
        <div className="Profile-info-div">
          <Info text="Eduardo Martins" />
          <Info text="Portugal, 23 years old" />
          <Info text="React Developer" />
          <Info text="915 018 506" />
          <Info text="eduardomartinspt@gmail.com" />
        </div>
      </div>
    );
  }
}

export default PersonalInfo;
