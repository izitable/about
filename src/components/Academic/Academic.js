import React, { Component } from "react";
import EducationItem from "../EducationItem/EducationItem";
import "./styles.css";

class Academic extends Component {
  constructor(props) {
    super(props);
    this.state = {
      education: [
        {
          description: "Foundation Year",
          begin_date: "Sept 2011",
          end_date: "May 2012",
          entity: "EF International Language Schools, Oxford"
        },
        {
          description: "Bachelor of Informatics Engineering",
          begin_date: "Sept 2012",
          end_date: "Aug 2015",
          entity: "Universidade de Aveiro"
        }
      ]
    };
  }

  render() {
    return (
      <div>
        <div class="Title-div">
          <h1 className="Title">Education</h1>
        </div>
        <div class="Info-container">
          {this.state.education.map(item => <EducationItem education={item} />)}
        </div>
      </div>
    );
  }
}

export default Academic;
