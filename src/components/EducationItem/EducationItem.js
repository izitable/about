import React, { Component } from "react";
import "./styles.css";

class EducationItem extends Component {
  render() {
    return (
      <div className="Education-div">
        <div className="Description-div">
          <p className="Education-title">{this.props.education.entity}</p>
          <p className="Education-type">{this.props.education.description}</p>

          <p className="Education-date">
            From {this.props.education.begin_date} to{" "}
            {this.props.education.end_date}
          </p>
        </div>
      </div>
    );
  }
}

export default EducationItem;
