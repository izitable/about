import React, { Component } from "react";
import "./styles.css";

class Info extends Component {
  render() {
    return (
      <div>
        <p className="Text">{this.props.text}</p>
      </div>
    );
  }
}

export default Info;
