import React, { Component } from "react";
import EducationItem from "../EducationItem/EducationItem";
import "./styles.css";

class Experience extends Component {
  constructor(props) {
    super(props);
    this.state = {
      education: [
        {
          description: "Internship",
          begin_date: "June 2012",
          end_date: "September 2012",
          entity: "Martifer"
        },
        {
          description: "Analyst",
          begin_date: "Sept 2015",
          end_date: "July 2016",
          entity: "Deloitte"
        },
        {
          description: "",
          begin_date: "Aug 2016",
          end_date: "Present",
          entity: "Freelancer"
        }
      ]
    };
  }

  render() {
    return (
      <div>
        <div class="Title-div">
          <h1 className="Title">Work Experience</h1>
        </div>
        <div class="Info-container">
        {this.state.education.map(item => <EducationItem education={item} />)}
        </div>
      </div>
    );
  }
}

export default Experience;
